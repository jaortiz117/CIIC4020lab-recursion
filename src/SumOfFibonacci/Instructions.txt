W. Sum of Fibonacci
time limit per test 1 second
memory limit per test 256 megabytes
input: standard input
output: standard output
Fibonacci sequence is defined as follows:

F(0) = 1 F(1) = 1 F(N) = F(n-1) + F(n-2)

Given number N, you need to represent F(N) as F(x) + F(y) + F(z) or tell that it is not possible.

Input
Single integer N(0 <= N <= 50)

Output
Three integers F(x), F(y), F(z) separated by single space or -1 if there is no answer

Examples
input
3
output
1 1 1

input
5
output
2 3 3