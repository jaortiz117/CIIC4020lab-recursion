package OrderedPairs;

import java.util.Scanner;

public class Main {
	
	private static int count = 0;

	public static void main(String[] args) {

		System.out.println("");
		Scanner in = new Scanner(System.in);
		long n = in.nextInt();
		
		Long arr[] = new Long[(int) n];
//		String s;
//		int i=0;
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = in.nextLong();
//			i++;
		}
		
		String result = pairFinder(arr);
		System.out.println(count + "\n"+ result);
		
	}

	private static String pairFinder(Long[] arr) {

		if(arr.length>1) {
			
			Long[] newArr = new Long[arr.length-1];
			
			for (int i = 1; i <= newArr.length; i++) {
				newArr[i-1] = arr[i];
			}
			//check first 2 elems
			if(arr[0]<arr[1]) {
				
				//if one meets crit add to count
				count++;
				
				//print elems
				return arr[0] + " " + arr[1] + "\n" + pairFinder(newArr);
				
			}
			
			return pairFinder(newArr);
		}
		return "";
	}

}
