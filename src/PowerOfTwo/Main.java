package PowerOfTwo;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		System.out.println("");
		Scanner in = new Scanner(System.in);
		long n = in.nextLong();
		
		if(isPower2(n)){
			System.out.println("yes");
		}
		else{
			System.out.println("no");
		}
	}

	private static boolean isPower2(Long n) {
		if(n == 1) return true;
		
		if(n%2==0) return isPower2(n/2);
		
		return false;
	}
}
