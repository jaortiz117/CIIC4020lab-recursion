package Fibonacci;

import java.util.Scanner;

public class Main {
	
	public static class Pair<E> {
		private E first; 
		private E second; 
		public E getFirst() { return first;}
		public void first(E first) { this.first = first;}
		public E second() { return second;}
		public Pair(E first, E second) {super();  this.first = first;  this.second = second; 
		}
	}
	    
	public static void main(String[] args) {

		System.out.println("");
		Scanner in = new Scanner(System.in);
		long n = in.nextLong();
		
		System.out.println(fibonacci(n).first);
		
	}	
	
	private static Pair<Long> fibonacci(long n) {
		
		if(n==0||n==1){ 
			return new Pair<Long>(1L,1L); 
		}

		else{

			Pair<Long> ab = fibonacci(n-1); 

			return new Pair<Long>(ab.first+ab.second, ab.first);
		}
	}

}
