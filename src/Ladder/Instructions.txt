Ladder
time limit per test 1 second
memory limit per test 256 megabytes
input: standard input
output: standard output

We shall define ladder as a structure of cubes, 
where each layer contains less cubes that the previous one. 
Given N, find number of stairs with N cubes. 

Input
A single number N(1 <= N <= 100)

Examples
input
3
output
2

input
6
output
4