package MergeSort;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		System.out.println("");
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		
		Long arr[] = new Long[n];
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = in.nextLong();
		}
		
		Queue<Long> q = arrToQueue(arr);
		sort(q);
		
		printSortedList(q);
		
	}

	private static void printSortedList(Queue<Long> q) {
		while(!q.isEmpty())
			System.out.print(q.poll()+" ");
		System.out.println("");
	}

	private static void sort(Queue<Long> q) {//merge sort halves the given list many times and then switches position based on this
		if(q.size() > 1) {
			Queue<Long> q1, q2;
			
			//init empty q1 and 2
			q1 = new LinkedList<>();
			q2 = new LinkedList<>();
			
			//split q into 2 halves
			for (int i = 0; i < q.size()/2; i++) {
				q1.offer(q.poll());
			}
			
			while(!q.isEmpty()) {
				q2.offer(q.poll());
			}
			
			sort(q1);//recursively sort
			sort(q2);
			
			merge(q, q1, q2);
		}
	}

	private static void merge(Queue<Long> q, Queue<Long> q1, Queue<Long> q2) {
		while (!q1.isEmpty() && !q2.isEmpty()) {
			if(((Comparable<Long>) q1.peek()).compareTo(q2.peek()) <= 0){
				q.offer(q1.poll());
			}
			else {
				q.offer(q2.poll());
			}
		}
		
		Queue<Long> r = (!q1.isEmpty() ? q1 : q2);// find which q1 or q2 is not empty yet//a condensed if else
		
		while(!r.isEmpty())
			q.offer(r.poll());
	}

	private static Queue<Long> arrToQueue(Long[] arr) {
		Queue<Long> q = new LinkedList<>();
		
		for (int i = 0; i < arr.length; i++) {
			q.offer(arr[i]);
		}
		
		return q;
	}

}
